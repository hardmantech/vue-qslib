import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';
import MatrixRowTitle from './MatrixRowTitle';
// 多题混合
export class MixedQuestionOption extends BaseOption {
    // 题号
    public ItemNo: string = '';
    public SelValue: boolean = false;
}
export default class MixedQuestionSubject extends BaseQuestionSubject<MixedQuestionOption> {
    public static option: MixedQuestionOption;

    public RowOptionText: string = '';
    // 行选项
    public RowOptions: MatrixRowTitle[] = [];
    // 0:多选，1，单选
    public QuestionType: number = 0;

}
